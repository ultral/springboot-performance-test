package test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import test.model.Data;
import test.model.DataRepository;

import javax.validation.Valid;

/**
 * Created by Konrad on 29.09.2016:15:09
 */
@RestController
public class MainController {
    @Autowired
    private DataRepository repository;

    @RequestMapping(path = "/ok", produces = "text/html")
    String ok() {
        return "ok";
    }

    @RequestMapping(path = "/response", produces = "text/html")
    String response(@Valid @RequestBody Data data) {
        return data.data;
    }

    @RequestMapping(path = "/", produces = "text/html")
    String start() {
        return "springboot-performance-test works";
    }

    @RequestMapping(path = "/data", method = RequestMethod.GET, produces = "application/json")
    Iterable<Data> getAll() {
        return repository.findAll();
    }

    @RequestMapping(path = "/data", method = RequestMethod.POST, produces = "application/json")
    Data postData(@Valid @RequestBody Data data) {
        return repository.save(data);
    }

    @RequestMapping(path = "/data", method = RequestMethod.PUT, produces = "application/json")
    Data putData(@Valid @RequestBody Data data) {
        return repository.save(data);
    }
}
