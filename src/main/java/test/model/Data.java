package test.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Data {

    @Id
    @GeneratedValue
    public Long id;

    @NotNull
    public String data;
}
