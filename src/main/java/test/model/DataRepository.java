package test.model;

import org.springframework.data.repository.CrudRepository;

/**
 * Created by Konrad on 29.09.2016:14:53
 */
public interface DataRepository extends CrudRepository<Data, Long> {}
